package org.languagedetection.service;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;
import org.languagedetection.db.ILanguageDB;
import org.languagedetection.db.InMemoryLanguageDB;
import org.languagedetection.exception.LanguageDetectException;
import org.languagedetection.language.LanguageFrequency;

import static org.hamcrest.Matchers.*;

public class LanguageDBServiceIT {
	
	private static ILanguageDB db;
	private static ILanguageDBService dbService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		db = new InMemoryLanguageDB.FileDBBuilder().build();
		dbService = new LanguageDBServiceImpl(db);
	}

	@Test
	public void languageDBShouldDetectWordsAppearedInThreeLanguagesProvided() throws LanguageDetectException {
		
		LanguageFrequency lf = dbService.detect(Arrays.asList("between", "electrique", "geschwatzt"));
		
		assertThat(lf.containsLanguage("english"), is(true));
		assertThat(lf.containsLanguage("french"), is(true));
		assertThat(lf.containsLanguage("german"), is(true));
	}
	
	@Test
	public void languageDBShouldOnlyDetectWordsAppearedInTwoLanguagesProvided() throws LanguageDetectException {
		
		LanguageFrequency lf = dbService.detect(Arrays.asList("between", "electrique"));
		
		assertThat(lf.containsLanguage("english"), is(true));
		assertThat(lf.containsLanguage("french"), is(true));
		assertThat(lf.containsLanguage("german"), is(false));
	}
	
	@Test
	public void languageDBShouldReturnAnEmptyLanguageFrequencyWhenWordsAreNotDetected() throws LanguageDetectException {
		
		LanguageFrequency lf = dbService.detect(Arrays.asList("abcde", "bedac"));
		
		assertThat(lf.containsLanguage("english"), is(false));
		assertThat(lf.containsLanguage("french"), is(false));
		assertThat(lf.containsLanguage("german"), is(false));
		assertThat(lf.isEmpty(), is(true));
	}
	
	@Test
	public void languageDBShouldAggregateLanguageFrequenciesWhenWordsAppearedInMultipleLanguages() throws LanguageDetectException {
		
		LanguageFrequency lf = dbService.detect(Arrays.asList("abcde", "between", "so"));
		
		assertThat(lf.containsLanguage("english"), is(true));
		assertThat(lf.containsLanguage("german"), is(true));
		assertThat(lf.containsLanguage("french"), is(false));
		assertThat(lf.getFrequency("german"), is(1));
		assertThat(lf.getFrequency("english"), is(2));
	}
	
	

}
