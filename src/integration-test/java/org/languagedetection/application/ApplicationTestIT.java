package org.languagedetection.application;

import static org.junit.Assert.*;

import org.junit.Test;
import org.languagedetection.exception.LanguageDetectException;

public class ApplicationTestIT {

	@Test
	public void whenNoArgumentFromTheInputTheApplicationShouldReturnSuccessfully() throws Exception {
		Application.main();
	}
	
	@Test (expected = LanguageDetectException.class)
	public void whenArgumentFromTheInputIsNotAFileApplicationShouldThrowException() throws Exception{
		
		String[] args = {"profile"};
		Application.main(args);
		// This should not be asserted
		assertTrue(true);
	}
	
	@Test (expected = LanguageDetectException.class)
	public void whenArgumentFromTheInputIsNotOnClassPathShouldThrowException() throws Exception{
		
		String[] args = {"NOT_ON_CLASSPATH.TXT"};
		Application.main(args);
		
		// This should not be asserted
		assertTrue(true);
	}
	
	@Test (expected = LanguageDetectException.class)
	public void whenLanguageDBBuilderErrorApplicationhShouldThrowException() throws Exception{
		
		String[] args = {"ENGLISH.1"};
		Application.mainTest(args);
		
		// This should not be asserted
		assertTrue(true);
	}
	
	
}
