package org.languagedetection.algorithm;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;

import org.junit.Test;
import org.languagedetection.db.InMemoryLanguageDB;
import org.languagedetection.exception.LanguageDetectException;
import org.languagedetection.service.ILanguageDBService;
import org.languagedetection.service.LanguageDBServiceImpl;

import static org.hamcrest.Matchers.*;

public class SimpleNavieAlgorithmTestIT {
	
	@Test
	public void simpleAlgorithmShouldReadCorrectNumberOfWordTokens() throws LanguageDetectException {
		ClassLoader classLoader = getClass().getClassLoader();
		URL urlResource = classLoader.getResource("CORRECT_NUM_TOKENS.TXT");

		File file = new File(urlResource.getFile());
		
		ILanguageDBService service = new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().build());
		
	    SimpleNaiveAlgorithm algorithm = new SimpleNaiveAlgorithm(file, service);
	    algorithm.execute();
	    assertThat(algorithm.getTotalWords(), equalTo(10));
	}
	
	@Test
	public void simpleAlgorithmShouldReturnAnEmptyResultWhenInputFileEmpty() throws LanguageDetectException {
		ClassLoader classLoader = getClass().getClassLoader();
		URL urlResource = classLoader.getResource("EMPTY_TOKENS.TXT");

		File file = new File(urlResource.getFile());
		
		ILanguageDBService service = new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().build());
		
	    SimpleNaiveAlgorithm algorithm = new SimpleNaiveAlgorithm(file, service);
	    Result result = algorithm.execute();
	    assertThat(result.size(), equalTo(0));
	    assertThat(algorithm.getTotalWords(), equalTo(0));
	}
	
	@Test
	public void simpleAlgorithmShouldReturnAnEmptyResultWhenWordsInFileCannotBeDetected() throws LanguageDetectException {
		ClassLoader classLoader = getClass().getClassLoader();
		URL urlResource = classLoader.getResource("CANNOT_DETECTED.TXT");

		File file = new File(urlResource.getFile());
		
		ILanguageDBService service = new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().build());
		
	    SimpleNaiveAlgorithm algorithm = new SimpleNaiveAlgorithm(file, service);
	    Result result = algorithm.execute();
	    assertThat(result.size(), equalTo(0));
	    assertThat(algorithm.getTotalWords(), equalTo(3));
	}
	
	@Test
	public void simpleAlgorithmShouldFrenchAsTheTopResultWhenAFrenchFileProvided() throws LanguageDetectException{
	   
		ClassLoader classLoader = getClass().getClassLoader();
		URL urlResource = classLoader.getResource("FRENCH.1");

		File file = new File(urlResource.getFile());
		
		ILanguageDBService service = new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().build());
		
	    SimpleNaiveAlgorithm algorithm = new SimpleNaiveAlgorithm(file, service);
	   
	    Result result = algorithm.execute(); 
	    
	    assertThat(result.top(), hasProperty("language", equalTo("french")));
	}
	
	@Test
	public void simpleAlgorithmShouldReturnGermanAsTheTopResultWhenAGermanFileProvided() throws LanguageDetectException{
	   
		ClassLoader classLoader = getClass().getClassLoader();
		URL urlResource = classLoader.getResource("GERMAN.1");

		File file = new File(urlResource.getFile());
		
		ILanguageDBService service = new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().build());
		
	    SimpleNaiveAlgorithm algorithm = new SimpleNaiveAlgorithm(file, service);
	   
	    Result result = algorithm.execute(); 
	    
	    assertThat(result.top(), hasProperty("language", equalTo("german")));
	}
	
	@Test
	public void simpleAlgorithmShouldReturnEnglishAsTheTopResultWhenAEnglishFileProvided() throws LanguageDetectException{
	   
		ClassLoader classLoader = getClass().getClassLoader();
		URL urlResource = classLoader.getResource("ENGLISH.1");

		File file = new File(urlResource.getFile());
		
		ILanguageDBService service = new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().build());
		
	    SimpleNaiveAlgorithm algorithm = new SimpleNaiveAlgorithm(file, service);
	   
	    Result result = algorithm.execute(); 
	    
	    assertThat(result.top(), hasProperty("language", equalTo("english")));
	}

}
