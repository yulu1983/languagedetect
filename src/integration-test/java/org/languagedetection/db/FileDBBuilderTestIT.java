package org.languagedetection.db;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.languagedetection.db.InMemoryLanguageDB.FileDBBuilder;
import org.languagedetection.exception.LanguageDetectException;

import static org.hamcrest.Matchers.*;

public class FileDBBuilderTestIT {

	private FileDBBuilder builder;
	
	@Before
	public void setUp() throws Exception {
	   builder = new InMemoryLanguageDB.FileDBBuilder();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void fileDBBuilderShouldUseDefaultProfilePathWhenNoProfilePathUsed() {
		assertThat(builder.getProfileFilePath(), is("profile")); 
	}
	
	@Test
	public void fileDBBuilderShouldUseProvidedPathIfPathProvided() {
		builder.profilePath("anotherPath");
		assertThat(builder.getProfileFilePath(), is("anotherPath"));
	}
	
	@Test (expected = LanguageDetectException.class)
	public void whenNullPathProvidedFileDBBuilderBuildShouldThrowException() throws Exception {
		builder.profilePath(null).build();
	}
	
	@Test (expected =  LanguageDetectException.class)
	public void whenEmptyPathProvidedFileDBBuilderBuildShouldThrowException() throws Exception {
		builder.profilePath("").build();
	}
	
	@Test(expected = LanguageDetectException.class)
	public void fileDBBuilderShouldThrowExceptionWhenProfilePathIsNotFound() throws LanguageDetectException {
		builder.profilePath("anotherNonExistencePath");
		builder.build();
	}
	
	@Test
	public void fileDBBuilderShouldReadCorrectNumberOfLanguageFiles() throws LanguageDetectException {
	   builder.profilePath("profile").build();
	   assertThat(builder.getNumberOfFiles(), is(4));
	}
	
	@Test
	public void fileDBBuilderShouldReadCorrectNumberOfLanguage() throws LanguageDetectException {
		builder.profilePath("profile").build();
		assertThat(builder.getLanguages().size(), is(3));
		Set<String> set = new HashSet<>();
		set.add("english");
		set.add("french");
		set.add("german");
		
		Set<String> lSet = builder.getLanguages();
		
		lSet.stream().forEach(s -> assertTrue(set.contains(s)));
	}
	
	
}
