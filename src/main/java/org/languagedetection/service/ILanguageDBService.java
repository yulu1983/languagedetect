package org.languagedetection.service;

import java.util.List;

import org.languagedetection.language.LanguageFrequency;

/**
 * 
 * @author Yuan Lu
 * A language db service interface
 *
 */

public interface ILanguageDBService {
   /**
    * 
    * @param word word to be detected
    * @return LanguageFrequency the number of languages this word appears in
    */
   LanguageFrequency detect(String word);
   
   /**
    * 
    * @param words A list of words to be detected
    * @return the number of languages the list of words appear in
    */
   LanguageFrequency detect(List<String> words); 
}
