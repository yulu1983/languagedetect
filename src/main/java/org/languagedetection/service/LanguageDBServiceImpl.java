package org.languagedetection.service;

import java.util.List;
import java.util.Set;

import org.languagedetection.db.ILanguageDB;
import org.languagedetection.language.LanguageFrequency;

/**
 * 
 * @author Yuan Lu
 * Implementation of ILanguageDBService
 *
 */

public class LanguageDBServiceImpl implements ILanguageDBService{

	private ILanguageDB db;
	
	public LanguageDBServiceImpl(ILanguageDB db) {
		this.db = db;
	}

	@Override
	public LanguageFrequency detect(String word) {
		Set<String> set = db.detect(word);
		
		LanguageFrequency lf = new LanguageFrequency();
		
		for(String s : set) {
			lf.add(s);
		}
		return lf;
		
	}

	@Override
	public LanguageFrequency detect(List<String> words) {
		
		LanguageFrequency lf = new LanguageFrequency();
		
		for(String w : words) {
			Set<String> set = db.detect(w);
			for(String s: set) {
				lf.add(s);
			}
		}
		return lf;
	}
}
