package org.languagedetection.db;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

import org.languagedetection.exception.LanguageDetectException;
import org.languagedetection.util.LanguageDetectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * 
 * @author Yuan Lu
 * This is In Memory Language DB, which holds the language read from 
 * given language profile files from profile directory
 * 
 * It has an nested class FileDBBuilder, which is responsible of reading
 * the files, and parsing the tokens.
 * 
 * The in memory database represeted using a MultiMap data structure, which is
 * essentially can be seen as Map<String, Set<String>>
 * 
 * The key is each unique word token, the values is set of languages this token appears in
 *
 */

public class InMemoryLanguageDB implements ILanguageDB{
	
	Logger logger = LoggerFactory.getLogger(InMemoryLanguageDB.class);
	
	private Multimap<String, String> dbMap;
	
	public InMemoryLanguageDB(Multimap<String, String> map) {
		this.dbMap = map;
	}
	
	/**
	 * @param s the word to be detected
	 * @return Set<String> a set of languages the word appears in
	 */
	@Override
	public Set<String> detect(String s) {
		
		if(Strings.isNullOrEmpty(s))
		   return new HashSet<String>(0);	
		
		return (Set<String>)dbMap.get(s);
	}
	
	public static class FileDBBuilder {
		private static Logger logger = LoggerFactory.getLogger(FileDBBuilder.class);
		
		private String profilePath = "profile";
		private int numberOfProfilesRead;
		private Set<String> languages;

		public FileDBBuilder() {
			numberOfProfilesRead = 0;
			languages = new HashSet<>();
		}
		
		public FileDBBuilder profilePath(String profilePath) {
			this.profilePath = profilePath;
			return this;
		}

		/**
		 * 
		 * @return ILanguageDB an InMemoryDB in this case
		 * @throws LanguageDetectException
		 */
		public ILanguageDB build() throws LanguageDetectException {

			if(logger.isDebugEnabled())
			   logger.debug("start building db...");
			
			if(LanguageDetectionUtil.isNullOrEmpty(profilePath)) {
				logger.error("language profile path is null or empty");
				throw new LanguageDetectException("language profile path is null or empty");
			}
			
			HashMultimap<String, String> map = HashMultimap.create();
			File directory = new File(profilePath);
			
			/*
			 * Detect if the profile directory exists or not
			 */
			if(!directory.exists() || !directory.canRead() || !directory.isDirectory()) {
				if(logger.isDebugEnabled()) {
				   logger.debug("Profile directory " + profilePath + " exists: " + directory.exists());
				   logger.debug("Profile directory " + profilePath + " have read permission: " + directory.canRead());
				   logger.debug("Profile Directory " + profilePath + " is a directory"  + directory.isDirectory());
				}
				logger.error("Language DB profile directory cannot be read.");
				throw new LanguageDetectException("Language DB profile directory cannot be read.");
			}
			
			/*
			 * Read each file based on the predefined file format, e.g. "ENGLISH.1, ENGLISH.2, GERMAN.20"
			 */
			for(File file : directory.listFiles()) {
				
				Optional<String> op = LanguageDetectionUtil.isValidFileName(file.getName());
				if (op.isPresent()) {
					String language = op.get().toLowerCase();
					buildHelper(language, file, map);
					languages.add(language);
					++numberOfProfilesRead;
					if(logger.isDebugEnabled())
					   logger.debug("Language Profile file read: " + file.getName());
				}
			}
			if(logger.isDebugEnabled())
			   logger.debug("end building db");
			return new InMemoryLanguageDB(map);
		}
		
		public Set<String> getLanguages() {
			return languages;
		}

		/*
		 * Store each work token into the multimap structure
		 */
		private void buildHelper(String language, File file, Multimap<String, String> map) throws LanguageDetectException{
			try(Scanner sc = new Scanner(file)) {
				while(sc.hasNextLine()) {
					Iterable<String> it = LanguageDetectionUtil.split(sc.nextLine());
					Iterator<String> itor = it.iterator();
					itor.forEachRemaining(str -> map.put(str.toLowerCase(), language));
				}
			}
			catch(Exception e) {
				logger.error("FileDBBuilder::build, error when reading profile files: " + file.getName());
				throw new LanguageDetectException();
			}
		}

		/* package scope */
		/* for unit test or integration test purpose */
		int getNumberOfFiles() {
			return numberOfProfilesRead;
		}

		String getProfileFilePath() {
			return profilePath;
		}
	}
}
