package org.languagedetection.db;

/**
 * @author Yuan Lu
 * 
 * This is the interface that all language DB should implement
 * 
 * 
 */
import java.util.Set;

public interface ILanguageDB {

	/**
	 * @param s             String to be detected
	 * @return Set<String>  A set of languages this word appears in
	 */
	Set<String> detect(String word);
}
