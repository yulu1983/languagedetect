package org.languagedetection.exception;

/**
 * 
 * @author Yuan Lu
 * Application Specific Exception class
 */
public class LanguageDetectException extends Exception {

	/**
	 * default serial number
	 */
	private static final long serialVersionUID = 1L;

	public LanguageDetectException() { }
	
	public LanguageDetectException(String msg) {
		super(msg);
	}
	
	
}
