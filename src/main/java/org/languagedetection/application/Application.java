package org.languagedetection.application;

import java.io.File;

import org.languagedetection.exception.LanguageDetectException;
import org.languagedetection.language.LanguageCount;
import org.languagedetection.service.ILanguageDBService;
import org.languagedetection.service.LanguageDBServiceImpl;
import org.languagedetection.algorithm.Result;
import org.languagedetection.algorithm.SimpleNaiveAlgorithm;
import org.languagedetection.db.InMemoryLanguageDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Yuan Lu
 * 
 * This is the main driver of this application
 * 
 * Input: a file or lists of file identified by their relative path, should be on classpath
 * or absolute path
 *
 */

public class Application {

	private static Logger logger = LoggerFactory.getLogger(Application.class); 
	
	public static void main(String...args) throws Exception {
		 
		if(args.length == 0) {
			logger.info("no input file");
			return;
		}
		
		ILanguageDBService service = new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().build());
		
		 for(String fileName : args) {
			File file = new File(fileName);
	    	if(file.exists() && file.isFile() && file.canRead()) {
	    		Result result = new SimpleNaiveAlgorithm(file, service).execute();
	    		if(result.size() > 0) {
	    			LanguageCount<Double> lc = result.top();
	    			logger.info("top detection result is: " + lc.getLanguage());
	    		}
	    		else
	    			logger.info("cannot detect language");
	    	}
	    	else {
	    		logger.error("Input file " + fileName + " exist: " + file.exists());
	    		logger.error("Input file: " + fileName + " is file: " + file.isFile());
	    		logger.error("Input file: " + fileName + " have read permission: " + file.canRead());
	    		throw new LanguageDetectException("The input file is not a file or cannot be read, please check file or permission");	
	    	}
	    		
	    }
		 
	 }
	
	 static void mainTest(String...args) throws Exception {
		 // simulate DB build error on start up
		 new LanguageDBServiceImpl(new InMemoryLanguageDB.FileDBBuilder().profilePath("nonexistence").build());
	 }
}
