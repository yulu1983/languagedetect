package org.languagedetection.algorithm;

import org.languagedetection.exception.LanguageDetectException;

/**
 * 
 * @author Yuan Lu
 * This is the interface used for implementing 
 * synchronized detection algorithm, where the caller is blocked
 * by the computation util the algorithm finishes.
 * 
 * SimpleNaiveAlgorithm implements this interface
 *
 */
public interface SyncAlgorithm {

	Result execute() throws LanguageDetectException;
}
