package org.languagedetection.algorithm;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.languagedetection.exception.LanguageDetectException;
import org.languagedetection.language.LanguageCount;
import org.languagedetection.language.LanguageFrequency;
import org.languagedetection.service.ILanguageDBService;
import org.languagedetection.util.LanguageDetectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Yuan Lu
 * This is a simple and Naive algorithm which caculates total language percentage
 * base on each word appears in which language
 *
 */
public class SimpleNaiveAlgorithm implements SyncAlgorithm {
	
   private static Logger logger = LoggerFactory.getLogger(SimpleNaiveAlgorithm.class);
	
   private ILanguageDBService service;
   private File inputFile;
   private final Map<String, Integer> map = new HashMap<String, Integer>();
   private int totalWords;
	
   public SimpleNaiveAlgorithm(File inputFile, ILanguageDBService service) throws LanguageDetectException {
	   if(inputFile == null || !inputFile.exists()) {
		   throw new LanguageDetectException("Input file does not exist");
	   }
	   this.service = service;
	   this.inputFile = inputFile;
	   this.totalWords = 0;
   }
   
   /**
    * This is a sync execution which reads the token from the file
    * and detect languages of lists of words
    */
   public Result execute() throws LanguageDetectException {
	   
	   try(Scanner sc = new Scanner(inputFile)) {
		   while(sc.hasNextLine()) {
				Iterable<String> it = LanguageDetectionUtil.split(sc.nextLine());
				List<String> words = LanguageDetectionUtil.iterableToList(it);
				if(!words.isEmpty()) {
					totalWords += words.size();
					LanguageFrequency lf = service.detect(words);
					aggregateResult(lf);
				}
			}
		    return calcResult();
	   }
	   catch(Exception e) {
		   logger.error("SimpleNaiveAlgorithm::run, Reading Input File Error: " + inputFile.getName());
		   throw new LanguageDetectException(); 
	   } 
   }
   
   /**
    * 
    * @return Result final detection result
    */
   private Result calcResult() {
	   
	   Iterator<Entry<String, Integer>> it = map.entrySet().iterator();
	   List<LanguageCount<Double>> list = new ArrayList<>();
	   while(it.hasNext()) {
		   Entry<String, Integer> e = it.next();
		   String language = e.getKey();
		   double percentage = e.getValue().intValue() / (double) totalWords;
		   list.add(new LanguageCount<Double>(language, percentage));
		   logger.info(language + ":" + percentage);
	   }
	   List<LanguageCount<Double>> reverseSortedList = list.stream().sorted((l1, l2) -> l2.getCount().compareTo(l1.getCount())).collect(Collectors.toList());;
	   
	   return new Result(reverseSortedList);
   }
   
   /**
    * Aggregates each intermediated Language Frequency
    */
   private void aggregateResult(LanguageFrequency lf) {
	   for(LanguageCount<Integer> lc : lf) {
		   if(map.containsKey(lc.getLanguage()))
			   map.put(lc.getLanguage(), map.get(lc.getLanguage()) + lc.getCount());
		   else
			   map.put(lc.getLanguage(), lc.getCount());
	   }
   }
   
   /* used for integration test */
   int getTotalWords() {
	   return totalWords;
   }
}
