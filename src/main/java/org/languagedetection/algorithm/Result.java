package org.languagedetection.algorithm;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.languagedetection.exception.LanguageDetectException;
import org.languagedetection.language.LanguageCount;

/**
 * 
 * @author Yuan Lu
 * This is a value object that store the detection result
 * multi-thread safe
 */

public class Result implements Serializable {

	/**
	 * default serialVersion number
	 */
	private static final long serialVersionUID = 1L;

	private final List<LanguageCount<Double>> results;
	
	public Result(List<LanguageCount<Double>> results) {
		this.results = results;
	}
	
	public int size() {
		return results.size();
	}
	
	/**
	 * 
	 * @return LanguageCount<Double>   The top ranked detected language
	 * @throws LanguageDetectException when no language detected
	 */
	
	public LanguageCount<Double> top() throws LanguageDetectException {
	   if(size() == 0)
		   throw new LanguageDetectException("Empty Detection Result");
	   
	   return results.get(0);
	}
	

	/**
	 * 
	 * @param n top n detected result
	 * @return List<LanguageCount<Double> top n ranked detected language based percentage
	 * @throws LanguageDetectException when no language detected
	 */
	public List<LanguageCount<Double>> topN(int n) throws LanguageDetectException {
		if(n <= 0)
			throw new LanguageDetectException("Empty Detection Result");
		return results.stream().limit(n).collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
	   return "[" + results.stream()
			     .map(i -> i.toString())
			     .collect(Collectors.joining(", ")) + "]";
			 
	}
	
}
