package org.languagedetection.language;
/**
 * 
 * @author Yuan Lu
 *
 * @param <T>
 * 
 * This is a value object used for language frequencies such as "english:10"
 * or for language percentage such as "english:0.98"
 * Immutable object, multi-thread safe
 * 
 */
public class LanguageCount<T extends Number> {

	private final String language;
	private final T count;
	
	public LanguageCount(String language, T count) {
		this.language = language;
		this.count = count;
	}
	
	public String getLanguage() { return language; }
	public T getCount() { return count; }
	
	@Override
	public String toString() {
        return language + ":" + count.toString();
    }
}
