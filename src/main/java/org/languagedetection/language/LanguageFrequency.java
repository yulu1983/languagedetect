package org.languagedetection.language;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
/**
 * 
 * @author Yuan Lu
 * 
 * An Iterable structure used to store the language and number of words
 * appeared in this language.
 * 
 * e.g. "english:8", words detected as english appears 8 times.
 * 
 * This class is not multithread safe
 *
 */

public class LanguageFrequency implements Iterable<LanguageCount<Integer>> {

	private final Map<String, Integer> map;
	
	public LanguageFrequency() {
		this.map = new HashMap<String, Integer>();
	}
	
	public void add(String language) {
		if(map.containsKey(language)) {
			map.put(language, map.get(language) + 1);
		}
		else 
		   map.put(language, 1);
	}
	
	public boolean containsLanguage(String language) {
		return map.containsKey(language);
	}
	
	public int getFrequency(String language) {
		Integer i = map.get(language);
		if(i == null)
			return 0;
		return i;
	}
	
	public boolean isEmpty() {
		return map.isEmpty();
	}
	
	public int size() {
		return map.size();
	}
	
	private class LanguageFrequencyIterator implements Iterator<LanguageCount<Integer>> {

		private final Iterator<Entry<String, Integer>> it;
		
		public LanguageFrequencyIterator(Map<String, Integer> map) {
           it = map.entrySet().iterator();	
		}
		
		@Override
		public boolean hasNext() {
			return it.hasNext();
		}

		@Override
		public LanguageCount<Integer> next() {
			Entry<String, Integer> e = it.next();
			return new LanguageCount<Integer>(e.getKey(), e.getValue());
		}
	}

	@Override
	public Iterator<LanguageCount<Integer>> iterator() {
		return new LanguageFrequencyIterator(map);
	}
	
	

}
