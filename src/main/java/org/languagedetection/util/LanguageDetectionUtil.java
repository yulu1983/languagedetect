package org.languagedetection.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

/**
 * @author Lu
 * An util class used through out this application
 */
public class LanguageDetectionUtil {
	
	private final static Pattern FILENAMEPATTERN = Pattern.compile("(\\w+)\\.\\d+");
	private final static String PUNCTUATION = " :;,.";
	
	/**
	 * check whether the String is Null or Empty
	 * @param s
	 * @return
	 */
	public static boolean isNullOrEmpty(String s) {
		return Strings.isNullOrEmpty(s);
	}
	
	/**
	 * check whether the file name is valid based on the predefined file name format
	 * @param s
	 * @return
	 */
	public static Optional<String> isValidFileName(String s) {
		Matcher mattcher = FILENAMEPATTERN.matcher(s);
		if(mattcher.matches())
			return Optional.of(mattcher.group(1));
		
		return Optional.empty();
	}
	
	
	/**
	 * @param w  String to be splitted
	 * @return Iterable<String>  A list of Tokens
	 * 
	 * This method is null safe, when w is empty or null
	 * the method returns an empty Iterable<String>
	 */
	public static Iterable<String> split(String w) {
		
		if(isNullOrEmpty(w))
			return new ArrayList<String>(0);
		
		return Splitter.on(CharMatcher.anyOf(PUNCTUATION)).trimResults()
		.omitEmptyStrings()
		.split(w);
	}
	
	/**
	 * Convert an unknown Iterable<String> into a List<String>
	 * @param it Iterable of String
	 * @return List<String> lists of Strings
	 */
	public static List<String> iterableToList(Iterable<String> it) {
		
		List<String> list = new ArrayList<String>();
		
		Iterator<String> itor = it.iterator();
		
		while(itor.hasNext()) {
			list.add(itor.next());
		}
		
		return list;
	}

}
