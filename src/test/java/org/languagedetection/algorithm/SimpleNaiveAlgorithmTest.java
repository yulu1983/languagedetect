package org.languagedetection.algorithm;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;
import org.languagedetection.algorithm.SimpleNaiveAlgorithm;
import org.languagedetection.exception.LanguageDetectException;
import org.languagedetection.service.ILanguageDBService;

import static org.mockito.Mockito.*;

public class SimpleNaiveAlgorithmTest {

	@Test (expected = LanguageDetectException.class)
	public void whenFileObjectIsNullSimpleNaiveAlgorithmShouldThrowExceptionInConstructor() throws LanguageDetectException {
		try {
		   ILanguageDBService service = mock(ILanguageDBService.class);
		   File file = null;
		   SimpleNaiveAlgorithm sna = new SimpleNaiveAlgorithm(file, service);
		   sna.execute();
		}
		catch(LanguageDetectException e) {
			throw e;
		}
		assertFalse(false);
		
	}
	
	@Test (expected = LanguageDetectException.class)
	public void whenFileNotExistsSimpleNaiveAlgorithmShouldThrowExceptionInConstructor() throws LanguageDetectException {
		try {
		   ILanguageDBService service = mock(ILanguageDBService.class);
		   File file = new File("nonexistenceFile");
		   SimpleNaiveAlgorithm sna = new SimpleNaiveAlgorithm(file, service);
		   sna.execute();
		}
		catch(LanguageDetectException e) {
			throw e;
		}
		assertFalse(false);
		
	}

}
