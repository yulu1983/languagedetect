package org.languagedetection.algorithm;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.languagedetection.algorithm.Result;
import org.languagedetection.language.LanguageCount;
import org.languagedetection.exception.LanguageDetectException;

import static org.hamcrest.Matchers.*;

public class ResultTest {

	@Test
	public void anEmptyResultShouldReturnSizeOfZero() {
		List<LanguageCount<Double>> list = new ArrayList<>();
		Result r = new Result(list);
		assertThat(r.size(), is(0));
	}
	
	@Test(expected = LanguageDetectException.class)
	public void whenResultIsEmptyGetTopDetectedLanguageShouldThrowException() throws LanguageDetectException {
		try {
			List<LanguageCount<Double>> list = new ArrayList<>();
			Result r = new Result(list);
			r.top();
		}
		catch(LanguageDetectException e) {
			throw e;
		}
		assertFalse(false);
	}
	
	@Test
	public void whenResultIsEmptyGetTopNDetectedLanguageShouldReturnAnEmptyList() throws LanguageDetectException {
		
		List<LanguageCount<Double>> list = new ArrayList<>();
		Result r = new Result(list);
		List<LanguageCount<Double>> l = r.topN(5);
		assertThat(l.size(), is(0));
	}
	
	@Test(expected = LanguageDetectException.class)
	public void topNResultShouldThrowExceptionWhenNIsLessThanEqualToZero() throws LanguageDetectException {
		try {
			List<LanguageCount<Double>> list = new ArrayList<>();
			Result r = new Result(list);
			r.topN(-1);
		}
		catch(LanguageDetectException e) {
			throw e;
		}
		assertFalse(false);
	}
	
	@Test
	public void topNShouldReturnWholeResultListWhenNIsGreaterThanTheResultSize() throws LanguageDetectException {
		try {
			List<LanguageCount<Double>> list = new ArrayList<>();
			list.add(new LanguageCount<Double>("english", 1.0));
			list.add(new LanguageCount<Double>("french", 0.8));
			list.add(new LanguageCount<Double>("german", 0.7));
			list.add(new LanguageCount<Double>("spanish", 0.6));
			
			Result r = new Result(list);
			List<LanguageCount<Double>> rl = r.topN(5);
			assertThat(rl.size(), is(r.size()));
		}
		catch(LanguageDetectException e) {
			throw e;
		}
	}
	
	@Test
	public void topNShouldReturnNResultWhenNIsLessThanTheSizeOfTheResultList() throws LanguageDetectException {
		try {
			List<LanguageCount<Double>> list = new ArrayList<>();
			list.add(new LanguageCount<Double>("english", 1.0));
			list.add(new LanguageCount<Double>("french", 0.8));
			list.add(new LanguageCount<Double>("german", 0.7));
			list.add(new LanguageCount<Double>("spanish", 0.6));
			
			Result r = new Result(list);
			assertThat(r.size(), is(list.size()));
			List<LanguageCount<Double>> rl = r.topN(3);
			assertThat(rl.size(), is(3));
		}
		catch(LanguageDetectException e) {
			throw e;
		}
	}
	
	@Test
	public void toStringShouldReturnTheExpectedFormat() {
		
		List<LanguageCount<Double>> emptyList = new ArrayList<>();
		
		Result r = new Result(emptyList);
		assertThat(r.toString(), is("[]"));
		
		List<LanguageCount<Double>> list = new ArrayList<>();
		list.add(new LanguageCount<Double>("english", 1.0));
		list.add(new LanguageCount<Double>("french", 0.8));
		list.add(new LanguageCount<Double>("german", 0.7));
		list.add(new LanguageCount<Double>("spanish", 0.6));
		
		Result r1 = new Result(list);
		assertThat(r1.toString(), is("[english:1.0, french:0.8, german:0.7, spanish:0.6]"));
	}
}
