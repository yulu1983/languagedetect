package org.languagedetection.service;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;

import org.junit.Test;
import org.languagedetection.db.ILanguageDB;
import org.languagedetection.language.LanguageFrequency;

import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

public class LanguageDBServiceTest {

	private ILanguageDB db;
	private ILanguageDBService languageDBService;
	
	@Before
	public void setUp() throws Exception {
       db = mock(ILanguageDB.class);
	   languageDBService = new LanguageDBServiceImpl(db);
	}
	
	
	@Test
	public void languageDBServiceShouldReturnAnEmptyLanguageFrequencyWhenWordNotFoundInAnyOfTheLanguages() {
		Set<String> set = new HashSet<>();
		
		when(db.detect("abcde")).thenReturn(set);
		LanguageFrequency lf = languageDBService.detect("abcde");
		assertThat(lf.size(), is(0));
		assertThat(lf.iterator().hasNext(), is(false));
	}

	@Test
	public void languageDBServiceShouldReturnACollectionWithSizeOneWhenTheWordAppearsOnlyInSingleLanguage() {
		
		Set<String> set = new HashSet<>();
		set.add("english");
		
		when(db.detect("world")).thenReturn(set);
		LanguageFrequency lf = languageDBService.detect("world");
		assertThat(lf.containsLanguage("english"), is(true));
		assertThat(lf.getFrequency("english"), is(1));
	}
	
	@Test
	public void languageDBServiceShouldReturnACollectionOfLanguageWhenTheWordAppearsInMultipleLanguage() {
        
		Set<String> set = new HashSet<>();
		set.add("english");
		set.add("german");
		
		when(db.detect("so")).thenReturn(set);
		LanguageFrequency lf = languageDBService.detect("so");
		assertThat(lf.containsLanguage("english"), is(true));
		assertThat(lf.getFrequency("english"), is(1));
		assertThat(lf.containsLanguage("german"), is(true));
		assertThat(lf.getFrequency("german"), is(1));
	}
	
	@Test
	public void languageDBServiceShouldAggregateReturnedLanguageSet() {
		
		Set<String> set1 = new HashSet<>();
		set1.add("english");
		set1.add("german");
		
		Set<String> set2 = new HashSet<>();
		set2.add("english");
		
		Set<String> set3 = new HashSet<>();
		set3.add("english");
		set3.add("french");
				
		when(db.detect("so")).thenReturn(set1);
		when(db.detect("world")).thenReturn(set2);
		when(db.detect("cliche")).thenReturn(set3);
		LanguageFrequency lf = languageDBService.detect(Arrays.asList("so", "world", "cliche", "cliche"));
		assertThat(lf.containsLanguage("english"), is(true));
		assertThat(lf.getFrequency("english"), is(4));
		assertThat(lf.containsLanguage("german"), is(true));
		assertThat(lf.getFrequency("german"), is(1));
		assertThat(lf.containsLanguage("french"), is(true));
		assertThat(lf.getFrequency("french"), is(2));
	}

}
