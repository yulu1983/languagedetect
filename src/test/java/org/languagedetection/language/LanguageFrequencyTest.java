package org.languagedetection.language;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class LanguageFrequencyTest {

	@Test
	public void anEmptyLanguageFrequencyCollectionShouldBeEmpty() {
		LanguageFrequency lf = new LanguageFrequency();
		assertThat(lf.isEmpty(), is(true));
		assertThat(lf.size(), is(0));
	}
	
	@Test
	public void languageFrequencyShouldAddNewLanguage() {
		LanguageFrequency lf = new LanguageFrequency();
		lf.add("english");
		lf.add("french");
		
		assertThat(lf.size(), is(2));
		assertThat(lf.containsLanguage("english"), is(true));
		assertThat(lf.containsLanguage("french"), is(true));
		assertThat(lf.containsLanguage("german"), is(false));
	}
	
	@Test
	public void languageFrequencyShouldReturnFalseWhenLanguageNotPresentInCollection() {
		LanguageFrequency lf = new LanguageFrequency();
		lf.add("english");
		lf.add("french");		

		assertThat(lf.containsLanguage("german"), is(false));
	}
	
	@Test
	public void languageFrequencyShouldReturnZeroFrequencyWhenLanguageNotPresentInCollection() {
		LanguageFrequency lf = new LanguageFrequency();
		lf.add("english");
		lf.add("french");		

		assertThat(lf.getFrequency("german"), is(0));
	}
	
	@Test
	public void languageFrequencyShouldIncrementCountWhenExistingLanguageAdded() {
		LanguageFrequency lf = new LanguageFrequency();
		lf.add("english");
		lf.add("french");		
		lf.add("english");
		lf.add("french");
		
		assertThat(lf.size(), is(2));
		assertThat(lf.getFrequency("english"), is(2));
		assertThat(lf.getFrequency("french"), is(2));
	}
	
	@Test
	public void languageFrequencyIteratorShouldIterateThroughCollectionCorrectNumberOfTimes() {
		LanguageFrequency lf = new LanguageFrequency();
		lf.add("english");
		lf.add("french");
		lf.add("german");
		lf.add("english");
		lf.add("french");
		lf.add("german");
		
		Iterator<LanguageCount<Integer>> it = lf.iterator();
		int count = 0;
		while(it.hasNext()) {
			LanguageCount<Integer> lc = it.next();
			assertThat(lc.getCount(), is(2));
			++count;
		}
		assertEquals(count, 3);
	}

}
