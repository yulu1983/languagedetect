package org.languagedetection.language;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class LanguageCountTest {

	@Test
	public void languageCountIntegerVersionTest() {
		LanguageCount<Integer> lc = new LanguageCount<>("english", 1);
		assertThat(lc.getLanguage(), is("english"));
		assertThat(lc.getCount(), is(1));
		assertThat(lc.toString(), is("english:1"));
	}
	
	@Test
	public void languageCountDoubleVersionTest() {
		LanguageCount<Double> lc = new LanguageCount<>("english", 1.0);
		assertThat(lc.getLanguage(), is("english"));
		assertThat(lc.getCount(), is(1.0));
		assertThat(lc.toString(), is("english:1.0"));
	}
	
	@Test
	public void languageCountLambdaShouldSortTheListBasedOnCount() {
		List<LanguageCount<Double>> list = new ArrayList<>();
		
		LanguageCount<Double> lc = new LanguageCount<>("english", 1.0);
		LanguageCount<Double> lc1 = new LanguageCount<>("french", 0.8);
		LanguageCount<Double> lc2 = new LanguageCount<>("german", 0.7);
		
		list.add(lc); list.add(lc1); list.add(lc2);
		
		List<LanguageCount<Double>>l = list.stream().sorted((l1, l2) -> l1.getCount().compareTo(l2.getCount())).collect(Collectors.toList());
		assertThat(l.size(), is(3));
		assertThat(l.get(0).getLanguage(), is("german"));
		
	}
	
	@Test
	public void languageCountLambdaShouldReverseSortTheListBasedOnCount() {
		List<LanguageCount<Double>> list = new ArrayList<>();
		
		LanguageCount<Double> lc2 = new LanguageCount<>("german", 0.7);
		LanguageCount<Double> lc1 = new LanguageCount<>("french", 0.8);
		LanguageCount<Double> lc = new LanguageCount<>("english", 1.0);
		
		list.add(lc2); list.add(lc1); list.add(lc);
		
		List<LanguageCount<Double>>l = list.stream().sorted((l1, l2) -> l2.getCount().compareTo(l1.getCount())).collect(Collectors.toList());
		assertThat(l.size(), is(3));
		assertThat(l.get(0).getLanguage(), is("english"));
		
	}

}
