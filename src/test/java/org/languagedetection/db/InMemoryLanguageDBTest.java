package org.languagedetection.db;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Set;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.languagedetection.exception.LanguageDetectException;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import static org.hamcrest.Matchers.*;

public class InMemoryLanguageDBTest {

	private static ILanguageDB ldb;
	private static Multimap<String, String> map;
	
	@BeforeClass
	public static void beforeClass() throws Exception {
	   map = HashMultimap.create();
	   map.put("hello", "english");
	   map.put("world", "english");
	   map.put("cliche", "french");
	   map.put("cliche", "english");
	   ldb = new InMemoryLanguageDB(map);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		 map.clear();
	}

	@Test
	public void languageDBShouldReturnAnEmptyCollectionWhenDetectNullOrEmptyString() throws LanguageDetectException{
		
		Collection<String> set1 = ldb.detect("");
		assertThat(set1.size(), is(0));
		
		Collection<String> set2 = ldb.detect(null);
		assertThat(set2.size(), is(0));
	}
	
	
	@Test
	public void dbShouldReturnASingleLanguageWhenTheWordOnlyAppearInOneLanguage() throws LanguageDetectException {
		
		Set<String> set1 = ldb.detect("hello");
		assertThat(set1.size(), is(1));
		
		Set<String> set2 = ldb.detect("world");
		assertThat(set2.size(), is(1));
	}
	
	@Test
	public void dbShouldReturnAnEmptyListWhenTheWordNotAppearInAnyLanguage() throws LanguageDetectException {
		Set<String> set = ldb.detect("nonexistencestring");
		assertThat(set.size(), is(0));
	}
	
	@Test
	public void dbShouldReturnSingleLanguageWhenTheWordAppearsOnlyInOneLanguage() throws LanguageDetectException {
	
		Set<String> set = ldb.detect("world");
		assertThat(set.size(), is(1));
		assertThat(set.contains("english"), is(true));
		assertThat(set.contains("french"), is(false));
	}
	
	@Test
	public void dbShouldReturnAListOfLanguagesWhenTheWordAppearsInMultipleLanguage() throws LanguageDetectException {
	
		Set<String>  set = ldb.detect("cliche");
		assertThat(set.size(), is(2));
		assertThat(set.contains("english"), is(true));
		assertThat(set.contains("french"), is(true));
	}
	

}
