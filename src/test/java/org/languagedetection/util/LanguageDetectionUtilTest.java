package org.languagedetection.util;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class LanguageDetectionUtilTest {
	
	@Test
	public void whenStringIsNullOrEmptyLanguageUtilShouldReturnTrue() {
		boolean b1 = LanguageDetectionUtil.isNullOrEmpty("");
		boolean b2 = LanguageDetectionUtil.isNullOrEmpty(null);
		assertTrue(b1 && b2);
	}
	
	@Test
	public void splitterShouldReturnAnEmptyIterableWhenSplitAnEmptyString() {
		
		Iterable<String> it = LanguageDetectionUtil.split("");
		assertThat(it.iterator().hasNext(), is(false));
	}
	
	@Test
	public void splitterShouldReturnAnEmptyIterableWhenSplitAnNullString() {
		
		Iterable<String> it = LanguageDetectionUtil.split(null);
		assertThat(it.iterator().hasNext(), is(false));
	}

	@Test
	public void splitterShouldOnlyReturnWordTokensWithoutWhiteSpaceAndPuctuation() {
	
		Set<String> set = new HashSet<>();
		set.add("Am"); set.add("if");
		set.add("number"); set.add("no");
		set.add("up"); set.add("period");
		set.add("regard"); set.add("sudden");
		set.add("better");
		
		String s1 = ", Am if number;no: up period, regard sudden better.";
		
		Iterable<String> it = LanguageDetectionUtil.split(s1);
		it.iterator().forEachRemaining(str -> assertTrue(set.contains(str)));
	}
	
	@Test
	public void splitterShouldReturnAnEmptyIterableWhenNoWordsPresent() {
		String s1 = ", ;  : .";
		String s2 = "   ";
		
		Iterable<String> it1 = LanguageDetectionUtil.split(s1);
		assertThat(it1.iterator().hasNext(), is(false));
		
		Iterable<String> it2 = LanguageDetectionUtil.split(s2);		
		assertThat(it2.iterator().hasNext(), is(false));

	}
	
	
	@Test
	public void fileNameShouldMatchThePredefinedFormat() {
		Optional<String> isValid = LanguageDetectionUtil.isValidFileName("ENGLISH.1");
		assertTrue(isValid.isPresent());
		
		isValid = LanguageDetectionUtil.isValidFileName("ENGLISH.22");
		assertTrue(isValid.isPresent());
		
		isValid = LanguageDetectionUtil.isValidFileName("FRENCH.R");
		assertFalse(isValid.isPresent());
		
		isValid = LanguageDetectionUtil.isValidFileName("FRENCH.CC");
		assertFalse(isValid.isPresent());
		
		isValid = LanguageDetectionUtil.isValidFileName("ENGLISH");
		assertFalse(isValid.isPresent());
		
	}
	
	

}
