Please note: the code compiles and run on Oracle Java 8 VM
Tested and run on jdk1.8.20

****
cd into the project root directory.
****

1.
To run unit tests:
mvn clean test

2.
To run integration tests:
mvn -Pintegration-test clean verify

3.
To package the application into an executable jar
mvn clean package

4.
To run the application:
In Project root directory

java -jar target/languagedetectExecutable.jar ENGLISH.1 or

java -jar target/languagedetectExecutable.jar "An Absoulte file path"

or without input file or somthing else
java -jar target/languagedetectExecutable.jar
